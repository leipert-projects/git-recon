class GitRecon < Formula
  desc "Reconcile your local git repositories"
  homepage "https://gitlab.com/leipert-projects/git-recon"
  url "https://gitlab.com/leipert-projects/git-recon/-/releases/v1.1.3/downloads/homebrew-packaged.tar.gz"
  sha256 "6c7a79d541792ec2d367e25a67d9480b5b43e3ef04bd4bacaa6e79358041216b"
  license "MIT"

  def install
    bin.install "git-recon.sh" => "git-recon"
  end

  test do
    system "#{bin}/git-recon", "--version"
  end
end
