# git-recon

> `git-recon` is a tool for reconciling your local git repositories.

Over time developers tend to check out a lot of repositories.
Git has a lot of things to keep track of: files, branches, tags, stashes, submodules, worktrees.
Once it is time for a cleanup, how do you know whether this repo is safe to delete?
With `git-recon` you can quickly check the status of all of them.

- [Demo](#demo)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)

## Demo

![Demo of git-recon](https://gitlab.com/leipert-projects/git-recon/uploads/10cf3c68f70e7ee8461f9f010210fe75/demo.gif)

## Features

- one simple bash script
- quickly check the status of repositories
- fast and offline only ([should this change?](https://gitlab.com/leipert-projects/git-recon/-/issues/2))
- recursive mode: `--recursive` for checking the status of _all_ the repos!
- porcelain mode: `--porcelain` for machine readable output
- uses `fd` over `find` if installed for performance
- checks whether:
   - a remote is configured
   - all local branches are pushed
   - all local tags are pushed
   - you have any stashes
   - all files are committed
- worktree support
- submodules are checked via recursive mode, [could this be better?](https://gitlab.com/leipert-projects/git-recon/-/issues/1)

## Installation

### Homebrew

On macOS / linux you can use brew to install this

```bash
brew tap leipert-projects/git-recon https://gitlab.com/leipert-projects/git-recon.git
brew install git-recon
```

### Other systems

Download [git-recon.sh](./git-recon.sh), rename it to `git-recon` and put it in your path:

```bash
curl -o git-recon https://gitlab.com/leipert-projects/git-recon/-/raw/main/git-recon.sh
chmod a+x git-recon
```

## Usage

<!--USAGE:START-->
```
git-recon - reconcile your local git repos [version v1.1.3]

USAGE
    git recon [--version] [--help] [--recursive] [--porcelain]

OPTIONS
    --recursive   Recursively check subfolders, if the current directory is
                  NOT a git folder, this will be enabled by default
    --porcelain   Machine readable output
    --no-emoji    Disable emoji in output
    --online      Include operations that require online operations
    --offline     Force offline mode
    --help        Prints this help
    --version     Prints current version

CONFIGURATION
    You can configure git recon to always be recursive:

      git config --type bool --global recon.recursive true

    If you are not a fan of emoji:

      git config --type bool --global recon.emoji false

    You also can enable online mode by default:

      git config --type bool --global recon.online true

git-recon is a tool for reconciling your local git repositories.

git has a lot of things to keep track of: files, branches, tags, stashes,
submodules and worktrees.
With git-recon you can quickly check the status of all of them.
```
<!--USAGE:END-->
