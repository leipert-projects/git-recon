#!/usr/bin/env sh

NEW_VERSION=$1

echo "Installing curl and jq"
apk add curl jq

BASE_URL="https://gitlab.com/api/v4/projects/23152801/releases/v${NEW_VERSION}"

echo "Retrieving info from $BASE_URL"
LINK_ID=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "" | jq '.assets.links[0].id')

echo "Updating download link $LINK_ID"

curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --data name="Homebrew tarball" --data filepath="/homebrew-packaged.tar.gz" "$BASE_URL/assets/links/128327" | jq
