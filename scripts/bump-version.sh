#!/usr/bin/env sh

NEW_VERSION=$1

echo "Installing bash"
apk add bash

echo "new version $1"

echo "Updating script"
sed "s#v[0-9]\+.[0-9]\+.[0-9]\+#v$NEW_VERSION#g" git-recon.sh >git-recon_new.sh
mv git-recon_new.sh git-recon.sh
chmod a+x git-recon.sh

echo "Creating tarball"
rm -f homebrew-packaged.tar.gz
tar -czvf homebrew-packaged.tar.gz ./git-recon.sh
SHA_SUM=$(sha256sum homebrew-packaged.tar.gz | awk '{ print $1 }')

echo "Updating formula with $NEW_VERSION and $SHA_SUM"
sed "s#v[0-9]\+.[0-9]\+.[0-9]\+#v$NEW_VERSION#g" Formula/git-recon.rb |
    sed "s#sha256 \"[0-9a-f]\+\"#sha256 \"$SHA_SUM\"#g" \
        >Formula/git-recon_new.rb
mv Formula/git-recon_new.rb Formula/git-recon.rb

echo "Updating README"
{
    awk 'BEGIN {p=1};
     /<!--USAGE:START-->/ { p = 0 ; };
     p { print }' README.md
    echo "<!--USAGE:START-->"
    echo "\`\`\`"
    ./git-recon.sh --help
    echo "\`\`\`"
    awk 'BEGIN {p=0};
     /<!--USAGE:END-->/ { p = 1 ; };
     p { print }' README.md
} >README_new.md

mv README_new.md README.md

echo "Updating version in tests"
./git-recon.sh --version >test/version.txt
