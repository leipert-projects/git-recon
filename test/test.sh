#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

UPDATE_FLAG=false
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

while [ $# -ne 0 ]; do
    arg="$1"
    case "$arg" in
    --update)
        UPDATE_FLAG=true
        ;;
    *) ;;
    esac
    shift
done

function clean {
    sed 's#/.*/test/repos#_base#g' | sed 's#[a-f0-9]\{6,\}#\[hash\]#g'
}

function setup {
    rm -rf repos
    mkdir -p repos
    cd "repos" || exit 1
    git init
    git config --type bool recon.recursive false
    ## Setting up git repo which is in sync
    git clone ../../ git-synced
    cd git-synced || exit 1
    git checkout v0.1.0
    cd ../
    ## Setting up git repo with no remotes
    cp -r git-synced git-no-remote
    cd git-no-remote || exit 1
    git remote remove origin
    cd ../
    ## Setting up git repo with stashes
    cp -r git-synced git-stashes
    cd git-stashes || exit 1
    echo "TEST" >>"README.md"
    git stash
    cd ../
    ## Setting up unclean repo
    cp -r git-synced git-unclean
    cd git-unclean || exit 1
    echo "TEST" >"NEW_FILE"
    echo "# foo" >>git-recon
    rm README.md
    cd ../
    ## Setting up unpushed branches
    cp -r git-unclean git-unpushed
    cd git-unpushed || exit 1
    git checkout -b "unpushed-branch"
    git add .
    git commit -am "Message"
    git checkout v0.1.0
    cd ../
    ## Setting up unpushed tags
    cp -r git-synced git-tags
    cd git-tags || exit 1
    git tag -m "fake-tag-1" "fake-tag-1"
    git tag -m "fake-tag-2" "fake-tag-2"
    git checkout v0.1.0
    cd ../
    ## Setting up a worktree
    cd git-synced || exit 1
    git worktree add ../git-worktree v0.1.0
    cd ../
    ## Setting git multi repo
    mkdir git-multi
    cd git-multi || exit 1
    git init
    echo ".gitignore" >"git-*"
    git add .
    git commit -m "Init"
    cp -r ../git-synced git-synced
    cp -r ../git-synced git-synced-2
    cd ../
    ## Submodules
    cp -r git-synced git-submodules
    cd git-submodules || exit 1
    git submodule add "$DIR/repos/git-synced"
    cd ../
}

cd "$DIR" || exit 1

echo "Setting up test repos"
setup &>/dev/null

echo "Running checks"

../../git-recon.sh | clean >../default.txt.out
../../git-recon.sh --porcelain | clean >../porcelain.txt.out
../../git-recon.sh --recursive | clean >../recursive.txt.out
../../git-recon.sh --recursive --porcelain | clean >../recursive_porcelain.txt.out
../../git-recon.sh --version | clean >../version.txt.out
../../git-recon.sh --recursive --online | clean >../recursive_online.txt.out
../../git-recon.sh --recursive --porcelain --online | clean >../recursive_porcelain_online.txt.out
git config --type bool recon.emoji false
../../git-recon.sh --recursive | clean >../no-emoji.txt.out

cd ..

error=false

for file in *.txt; do
    echo "Checking $file "
    if [ "$UPDATE_FLAG" == "true" ]; then
        cp -f "$file.out" "$file"
        echo "Updated $file"
    else
        if diff -u "$file" "$file.out"; then
            printf "\t%s matches expectation\n" "$file"
        else
            error=true
        fi
    fi
done

if [ "$error" == "true" ]; then
    echo "One or more checks failed"
    exit 1
fi
