## [1.1.3](https://gitlab.com/leipert-projects/git-recon/compare/v1.1.2...v1.1.3) (2020-12-30)


### Bug Fixes

* **semantic-release:** Fix download path for formula ([660e98a](https://gitlab.com/leipert-projects/git-recon/commit/660e98a454a906b90a729d8c7139d5cc9b984b42))

## [1.1.2](https://gitlab.com/leipert-projects/git-recon/compare/v1.1.1...v1.1.2) (2020-12-30)


### Bug Fixes

* **semantic-release:** Hopefully final fix in our release madness ([5f83cf7](https://gitlab.com/leipert-projects/git-recon/commit/5f83cf78189c3d66e9a370a0c3e580433af78f54))

## [1.1.1](https://gitlab.com/leipert-projects/git-recon/compare/v1.1.0...v1.1.1) (2020-12-29)


### Bug Fixes

* Fix version in help and version commands ([258f9a1](https://gitlab.com/leipert-projects/git-recon/commit/258f9a1cf1e0015e50686736a69960406cf61bf4))

# [1.1.0](https://gitlab.com/leipert-projects/git-recon/compare/v1.0.1...v1.1.0) (2020-12-29)


### Bug Fixes

* Only compare tags in online mode ([cdff572](https://gitlab.com/leipert-projects/git-recon/commit/cdff572e79402b8ec7eb1dc6be3df8d93553710b)), closes [#5](https://gitlab.com/leipert-projects/git-recon/issues/5)
* **emoji:** Use different representation for emojis ([b680cfe](https://gitlab.com/leipert-projects/git-recon/commit/b680cfe36086febfe60859346b53e933a732ef7f)), closes [#3](https://gitlab.com/leipert-projects/git-recon/issues/3)


### Features

* **config:** Allow to set certain options with git-config ([f755f64](https://gitlab.com/leipert-projects/git-recon/commit/f755f6420815d92579541157f98f5c4768738877)), closes [#4](https://gitlab.com/leipert-projects/git-recon/issues/4)
